import json
import requests
import sys

def json_to_pls(url):
	r = requests.get(url)
	if r.status_code == 200:
		songs = r.json()
		f = open("playlist.pls", "w+")
		f.write("[playlist]\n")
		for i in range(0, len(songs)):
			song = songs[i]

			if song["file_url"] != False:
				f.write("File{}={}\n".format(i+1, song["file_url"]))
				if i == len(songs)-1:
					f.write("Title{}={}".format(i+1, song["title"]))
				else:
					f.write("Title{}={}\n\n".format(i+1, song["title"]))
				print("File{} processed".format(i+1))
		f.close()


if __name__ == "__main__":
	url = sys.argv[1]
	json_to_pls(url)
